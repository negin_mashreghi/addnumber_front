const express = require('express');
const path = require('path');

// Init app
const app = express();

app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname+'/index.html'));
    //res.send("test")
});

const port = 3000;
app.listen(port, () => console.log(`Server started on port ${port}`)); 

module.exports = app;